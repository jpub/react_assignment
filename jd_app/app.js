var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var dbHandler = require('./dbschema/dbHandler');

var indexRouter = require('./routes/index');
var authenticateRouter = require('./routes/apiAuthenticate');
var submissionRouter = require('./routes/apiSubmission');
var accountRouter = require('./routes/apiAccount');
var applicationsRouter = require('./routes/applications');

// connection to MongoAtlas
dbHandler.connect();


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/api/authenticate', authenticateRouter);
app.use('/api/submissions', dbHandler.checkToken, submissionRouter);
app.use('/api/accounts', dbHandler.checkToken, accountRouter);
//app.use('/api/submissions', submissionRouter);
app.use('/api/applications', dbHandler.checkToken, applicationsRouter);

// just a simple endpoint to check for token validity
app.get('/api/validateToken', dbHandler.checkToken, function(req, res) {
  res.sendStatus(200);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
