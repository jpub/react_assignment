var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var schUserprofile = require('./schUserprofile');

mongoose.set('useFindAndModify', false);

const jwtSecret = 'of course this cannot be here..just put it here for convenience..must shift to env in production';
const jwtPublicCookie = (new Date().getTime()).toString();

const dbHandler = (function () {
    var dbh;

    init = () => {
        console.log('db handler initing');
        // any private var or method we just put here

        // public var and methods put here
        dbh = {};

        dbh.connect = () => {
            const dbRoute = 'mongodb+srv://dummy:glmm8chine@cluster23-i9zgl.mongodb.net/travelagency?retryWrites=true&w=majority';
            //const dbRoute = 'mongodb://localhost/travelagency';
            mongoose.connect(dbRoute, {useNewUrlParser: true});
            mongoose.connection.once('open', () => console.log('connected to the database ' + dbRoute));
        };

        dbh.authenticate = (res, username, password) => {
            schUserprofile.findOne({
                $and: [
                    {username: username},
                    {password: password}
                ]
            }, (err, userprofile) => {
                if (err || !userprofile) {
                    console.log(err);
                    res.status(401).json({
                        msg: 'fail authentication'
                    });
                } else {
                    // the payload only consist of username
                    console.log('encrypting...' + userprofile.username);
                    const jwtPayload = {
                        uid: userprofile.username,
                        jpc: jwtPublicCookie
                    };
                    // note that during signing the payload must be an object..it cannot just be jswPayload
                    const jwtToken = jwt.sign(
                        jwtPayload,
                        jwtSecret,
                        {expiresIn: '2h'}
                    );
                    // set to false so that we can use javascript to clear the cookie to invalidate the session
                    res.cookie('jwtPC', jwtPublicCookie, {httpOnly: false});
                    res.cookie('jwtToken', jwtToken, {httpOnly: true}).sendStatus(200);
                }
            });
        };

        // pass this middleware to express to ensure all api access are always verified
        dbh.checkToken = (req, res, next) => {
            const token =
                req.body.jwtToken ||
                req.query.jwtToken ||
                req.headers['x-access-token'] ||
                req.cookies.jwtToken;

            if (!token) {
                res.status(401).json({
                    msg: 'authorization needed'
                });
            } else {
                jwt.verify(token, jwtSecret, (err, decoded) => {
                    if (err || !req.cookies.jwtPC) {
                        res.status(401).json({
                            msg: 'authorization needed'
                        });
                    } else {
                        // check if the public cookie is still valid
                        if (req.cookies.jwtPC === decoded.jpc) {
                            req.username = decoded.uid;
                            next();
                        } else {
                            res.status(401).json({
                                msg: 'authorization needed'
                            });
                        }
                    }
                });
            }
        };

        // so that we can chain it easier at the submission api layer to auto gen id
        dbh.countSchema = (schema) => {
            return new Promise((resolve, reject) => {
                schema.countDocuments({}, (err, count) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(count);
                    }
                });
            });
        };

        dbh.getAllBy = (req, res, schema, criteria) => {
            schema.find(criteria, (err, documents) => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        msg: err.toString()
                    });
                } else {
                    res.status(200).json(documents);
                }
            });
        };

        dbh.getOneBy = (req, res, schema, criteria) => {
            schema.findOne(criteria, (err, document) => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        msg: err.toString()
                    });
                } else {
                    res.status(200).json(document);
                }
            });
        };

        dbh.updateOneBy = (req, res, schema, criteria, data) => {
            console.log('trying...findOneAndUpdate');
            console.log(data);
            schema.findOneAndUpdate(criteria, data, {upsert: false}, (err, document) => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        msg: err.toString()
                    });
                } else {
                    res.status(200).json(document);
                    console.log(document);
                }
            });
        };

        dbh.insert = (req, res, schema, data) => {
            const record = new schema(data);
            record.save( (err) => {
                if (err) {
                    console.log(err);
                    res.status(500).json({
                        msg: err.toString()
                    });
                } else {
                    res.sendStatus(200);
                }
            });
        };

        return dbh;
    };

    if (!dbh) {
        dbh = init();
    }
    return dbh;
})();

// rightfully module.exports can already resolve the filename to ensure singleton
module.exports = dbHandler;
