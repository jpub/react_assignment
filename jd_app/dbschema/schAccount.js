var mongoose = require('mongoose');

const DataSchema = new mongoose.Schema(
    {
        username: String,
        applicantName: String,
        applicantContact: String,
        applicantEmail: String,
        companyName: String,
        companyAddress: String,
        companyUen: String
    }
);

module.exports = mongoose.model('account', DataSchema);