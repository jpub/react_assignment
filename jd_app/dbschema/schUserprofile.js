var mongoose = require('mongoose');

const DataSchema = new mongoose.Schema(
    {
        username: String,
        password: String
    }
);

// must be singular name! Mongo will create a plural collection of this name automatically
module.exports = mongoose.model('userprofile', DataSchema);