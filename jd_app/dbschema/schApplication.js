var mongoose = require('mongoose');

const DataSchema = new mongoose.Schema(
    {
        userid: String,
        companyName: String,
        companyAddress: String,
        companyUen: String,
        applicantName: String,
        applicantContact: String,
        applicantEmail: String,
        employeeName: String,
        employeeNric: String,
        employeePassport: String,
        employeeCountryOrigin: String,
        employeeCountryDestination: String,
        employeeTravelPeriod: String
    }
);

module.exports = mongoose.model("applications", DataSchema);