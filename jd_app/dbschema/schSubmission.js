var mongoose = require('mongoose');

const DataSchema = new mongoose.Schema(
    {
        username: String,
        submissionDate: String,
        submissionId: Number,
        submissionStatus: String,
        submissionAmount: Number,
        submissionDueOn: String,
        submissionPayOn: String,
        applicantName: String,
        applicantContact: String,
        applicantEmail: String,
        companyName: String,
        companyAddress: String,
        companyUen: String,
        employeeName: String,
        employeeNric: String,
        employeePassportNum: String,
        employeeCountryOrigin: String,
        employeeCountryDestination: String,
        employeeTravelDateStart: String,
        employeeTravelDateEnd: String
    }
);

module.exports = mongoose.model('submission', DataSchema);