var express = require('express');
var router = express.Router();
var dbHandler = require('../dbschema/dbHandler');
var schAccount = require('../dbschema/schAccount');

router.get('/', (req, res) => {
    // user name is from jwt so that current user can never see other user records
    dbHandler.getOneBy(req, res, schAccount, {username: req.username});
});

router.post('/', (req, res) => {
    //dbHandler.authenticate(res, username, req.body);
    dbHandler.updateOneBy(req, res, schAccount, {username: req.username}, req.body);
});

module.exports = router;