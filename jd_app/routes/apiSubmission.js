var express = require('express');
var router = express.Router();
var dbHandler = require('../dbschema/dbHandler');
var schSubmission = require('../dbschema/schSubmission');

router.get('/', (req, res) => {
    // user name is from jwt so that current user can never see other user records
    dbHandler.getAllBy(req, res, schSubmission, {username: req.username});
});

router.post('/', (req, res) => {
    dbHandler.countSchema(schSubmission)
        .then((result) => {
            let data = req.body;
            // new entry
            if (data.submissionId === -1) {
                data.username = req.username;
                data.submissionId = result + 1;
                data.submissionAmount = Math.random() * (1000 + 2000) + 1000;
                dbHandler.insert(req, res, schSubmission, data);
            } else {
                data.username = req.username;
                data.submissionId = req.body.submissionId;
                dbHandler.updateOneBy(
                    req,
                    res,
                    schSubmission, {
                        $and: [
                            {username: req.username},
                            {submissionId: req.body.submissionId}
                        ]
                    }, data);
            }
        })
        .catch((err) => {
            res.sendStatus(500);
        });


    //dbHandler.updateOneBy(req, res, schSubmission, {username: req.username}, req.body);
});

module.exports = router;