var express = require('express');
var router = express.Router();
var dbHandler = require('../dbschema/dbHandler');
var schApplication = require('../dbschema/schApplication');

// GET the applications
router.get('/:userid', function (req, res, next) {
    //console.log(req.query);
    //console.log(req.params);
    console.log('trying to get from db');
    const initialState = dbHandler.getAllBy(req, res, schApplication, {userid: req.params.userid});
    //console.log('wtf');
    //console.log(initialState);
    // res.render('applications', {
    //     opsType: "GET",
    //     incomingParams: JSON.stringify(req.params),
    //     incomingQuery: JSON.stringify(req.query),
    //     incomingBody: JSON.stringify(req.body)
    // });
    // const initialState = {
    //     companyName: 'ggg',
    //     companyAddress: 'c2ddda',
    //     companyUen: 'c3',
    //     applicantName: 'a1',
    //     applicantContact: 'a2',
    //     applicantEmail: 'a3',
    //     employeeName: 'e1gggg',
    //     employeeNric: 'e2',
    //     employeePassport: 'e3',
    //     employeeCountryOrigin: 'e4',
    //     employeeCountryDestination: 'e5',
    //     employeeTravelPeriod: 'e6'
    // };
    //res.json(initialState);
});

// POST the application - crete new application
router.post('/', function (req, res) {
    console.log(req.body);
    res.render('applications', {
        opsType: "POST",
        incomingParams: JSON.stringify(req.params),
        incomingQuery: JSON.stringify(req.query),
        incomingBody: JSON.stringify(req.body)
    });
});

// PUT the application - update the application
router.put('/', function (req, res) {
    console.log(req.body);
    res.render('applications', {
        opsType: "PUT",
        incomingParams: JSON.stringify(req.params),
        incomingQuery: JSON.stringify(req.query),
        incomingBody: JSON.stringify(req.body)
    });
});

// DELETE the application - remove the application
router.delete('/:id', function (req, res) {
    console.log('what thd hec');
    console.log(req.body);
    res.render('applications', {
        opsType: "DELETE",
        incomingParams: JSON.stringify(req.params),
        incomingQuery: JSON.stringify(req.query),
        incomingBody: JSON.stringify(req.body)
    });
});

module.exports = router;