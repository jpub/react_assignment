var express = require('express');
var router = express.Router();
var dbHandler = require('../dbschema/dbHandler');

router.post('/', (req, res) => {
   const {username, password} = req.body;
   dbHandler.authenticate(res, username, password);
});

router.get('/check', (req, res) => {
   res.sendStatus(200);
});

module.exports = router;