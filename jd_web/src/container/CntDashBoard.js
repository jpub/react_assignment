import React, {Component} from 'react';
import {connect} from 'react-redux';
import CmpHeader from '../component/CmpHeader'
import CmpSubmissionDetails from "../component/CmpSubmissionDetails";
import jdUtil from "../jdUtil";
import axios from "axios";


class CntDashBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: null,
            isPendingPayment: false
        }
    }

    retrieveFromGateway() {
        axios.get('/api/submissions').then((response) => {
            // sort according to sumission id
            response.data.sort((a, b) => (a.submissionId > b.submissionId) ? -1 : 1);
            this.props.dispatch({
                type: 'SUBMISSION_FROM_GW',
                payload: response.data
            });
            // highlight the first entry on first load
            if (this.props.submissionDetails.length > 0) {
                const paymentPending = this.props.submissionDetails[0].submissionPayOn === '---' ? true : false;
                this.setState({
                    selectedIndex: 0,
                    isPendingPayment: paymentPending
                })
            }
        }).catch((error) => {
            //console.log(error);
        })
    }

    handleClickRow(index) {
        const paymentPending = this.props.submissionDetails[index].submissionPayOn === '---' ? true : false;
        this.setState({
            selectedIndex: index,
            isPendingPayment: paymentPending
        })
    }

    getStatusClassName(status) {
        if (status === 'pending')
            return 'siimple-table-cell siimple-table-cell-pending';
        else if (status === 'approve')
            return 'siimple-table-cell siimple-table-cell-approve';
    }

    componentDidMount() {
        this.retrieveFromGateway();
    }

    handleClickEdit = () => {
        this.props.dispatch({
            type: 'SUBMISSION_UPDATE',
            payload: this.props.submissionDetails[this.state.selectedIndex]
        })
        this.props.history.push({
            pathname: '/application',
        });
    };

    render() {
        let paymentButton = null;
        let editButton = null;
        if (this.state.isPendingPayment) {
            paymentButton = (
                <div style={{marginTop: '20px'}} className="siimple-btn siimple-btn--success"
                     onClick={this.handleClickSubmit}>Pay Now
                </div>
            );
            editButton = (
                <div style={{marginTop: '20px', marginLeft:'20px'}} className="siimple-btn siimple-btn--primary"
                     onClick={this.handleClickEdit}>Edit
                </div>
            );
        }
        return (
            <div>
                <CmpHeader currentPage="Submission"/>
                <div className="content">
                    <div className="siimple-grid">
                        <div className="siimple-grid-row">
                            <div className="siimple-grid-col siimple-grid-col--3 siimple-grid-col--sm-12">
                                <div className="jd-box">
                                    <div className="jd-form-title">Submission Details</div>
                                    <CmpSubmissionDetails submissionDetails={
                                        this.state.selectedIndex == null ? null : this.props.submissionDetails[this.state.selectedIndex]
                                    }/>
                                    {paymentButton}{editButton}
                                </div>
                            </div>
                            <div className="siimple-grid-col siimple-grid-col--9 siimple-grid-col--sm-12">
                                <div className="siimple-grid-row">
                                    <div className="jd-form-title">Submission Records</div>
                                    <div className="jd-spacer"/>
                                    <div
                                        className="siimple-table siimple-table--hover siimple-table--border siimple-table-cus">
                                        <div className="siimple-table-header">
                                            <div className="siimple-table-row siimple-table-row--primary">
                                                <div className="siimple-table-cell">Date</div>
                                                <div className="siimple-table-cell">Name</div>
                                                <div className="siimple-table-cell">Passport</div>
                                                <div className="siimple-table-cell">Origin</div>
                                                <div className="siimple-table-cell">Destination</div>
                                                <div className="siimple-table-cell">DateStart</div>
                                                <div className="siimple-table-cell">DateEnd</div>
                                                <div className="siimple-table-cell">Amount</div>
                                                <div className="siimple-table-cell">Status</div>
                                            </div>
                                        </div>
                                        <div className="siimple-table-body">
                                            {this.props.submissionDetails.map((x, index) => {
                                                return (
                                                    <div key={index}
                                                         className={this.state.selectedIndex === index ? 'siimple-table-row siimple-table-row-selected' : 'siimple-table-row'}
                                                         onClick={() => {
                                                             this.handleClickRow(index);
                                                         }}>
                                                        <div
                                                            className="siimple-table-cell">{jdUtil.getDisplayDateFromStr(x.submissionDate)}</div>
                                                        <div className="siimple-table-cell">{x.employeeName}</div>
                                                        <div
                                                            className="siimple-table-cell">{x.employeePassportNum}</div>
                                                        <div
                                                            className="siimple-table-cell">{x.employeeCountryOrigin}</div>
                                                        <div
                                                            className="siimple-table-cell">{x.employeeCountryDestination}</div>
                                                        <div
                                                            className="siimple-table-cell">{jdUtil.getDisplayDateFromStr(x.employeeTravelDateStart)}</div>
                                                        <div
                                                            className="siimple-table-cell">{jdUtil.getDisplayDateFromStr(x.employeeTravelDateEnd)}</div>
                                                        <div
                                                            className="siimple-table-cell">{jdUtil.getDisplayCurrency(x.submissionAmount)}</div>
                                                        <div
                                                            className={this.getStatusClassName(x.submissionStatus)}>
                                                            {x.submissionStatus}
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = reduxState => {
    return {
        loginDetails: reduxState.loginDetails,
        submissionDetails: reduxState.submissionDetails,
        employeeDetails: reduxState.employeeDetails
    }
};

export default connect(mapStateToProps)(CntDashBoard);
