import React, {Component} from 'react';
import {connect} from 'react-redux';
import DatePicker from 'react-datepicker';
import CmpHeader from '../component/CmpHeader';
import CmpCompanyDetails from '../component/CmpCompanyDetails';
import CmpApplicatnDetails from '../component/CmpApplicantDetails'
import 'react-datepicker/dist/react-datepicker.min.css';
import jdUtil from "../jdUtil";
import axios from "axios";
import submissionUpdate from "../reducer/rdcSubmissionUpdate";

var updeep = require('updeep');


class CntNewApplication extends Component {
    // put the input details as local state if not I will be flooding redux with actions for snapshots Im not interested in just to have 2-way binding
    constructor(props) {
        super(props);

        this.state = {
            isAllEntryValid: false,
            validationInputFields: [
                {employeeName: ['Name', true, true, '']}, // fieldName - mandatory - isValid - errStr
                {employeeNric: ['NRIC', false, true, '']},
                {employeePassportNum: ['Passport Number', true, true, '']},
                {employeeCountryOrigin: ['Country of Origin', true, true, '']},
                {employeeCountryDestination: ['Country of Destination', true, true, '']}
            ],
            validationDateFields: [
                {employeeTravelDateStart: ['Period from', true, true, '']},
                {employeeTravelDateEnd: ['Period to', true, true, '']}
            ]
        }
    }

    retrieveFromGateway() {
        axios.get('/api/accounts').then((response) => {
            this.props.dispatch({
                type: 'ACCOUNT_FROM_GW',
                payload: response.data
            });
        }).catch((error) => {
            console.log(error);
        })
    }

    componentDidMount() {
        this.retrieveFromGateway();
    }

    // create a 2-way binding to redux
    handleChange(value, stateKeyName) {
        this.props.dispatch({
            type: 'EMPLOYEE_ENTRY_BINDING',
            payload: {
                stateKeyName: stateKeyName,
                inputValue: value
            }
        });
    };

    // validate all the required fields
    handleClickContinue = () => {
        let nextIsAllValid = true;
        // validate all mandatory input fields are not empty
        const nextValidationInputFields = this.state.validationInputFields.map(x => {
            const keyName = Object.keys(x)[0];
            const stateValue = this.props.employeeDetails[keyName];

            // set this field to valid and clear the error string
            if (!jdUtil.isStringEmpty(stateValue))
                return updeep({
                    [keyName]: {2: true, 3: ''}
                }, x);
            // field is blank or all white spaces
            else {
                const fieldDetails = Object.values(x)[0];
                // true means mandatory field
                if (fieldDetails[1]) {
                    nextIsAllValid = false;
                    return updeep({
                        [keyName]: {2: false, 3: 'Mandatory field'}
                    }, x);
                }
                return x;
            }
        });

        // validate all date fields
        const nextValidationDateFields = this.state.validationDateFields.map(x => {
            const keyName = Object.keys(x)[0];
            const stateValue = this.props.employeeDetails[keyName];
            // check for null
            if (!stateValue) {
                nextIsAllValid = false;
                return updeep({
                    [keyName]: {2: false, 3: 'Mandatory field'}
                }, x);
            }
            // check for historical flying date
            // if (stateValue < (new Date()).setHours(0, 0, 0, 0)) {
            //     nextIsAllValid = false;
            //     return updeep({
            //         [keyName]: {2: false, 3: 'Date has to be today or after'}
            //     }, x);
            // }
            // check for illogical order
            if (keyName === 'travelDateEnd' && this.props.employeeDetails.travelDateStart) {
                if (stateValue < this.props.employeeDetails.travelDateStart) {
                    nextIsAllValid = false;
                    return updeep({
                        [keyName]: {2: false, 3: 'Return date has to be same as departure date or after'}
                    }, x);
                }
            }
            // return valid field
            return updeep({
                [keyName]: {2: true, 3: ''}
            }, x);
        });

        this.setState({isAllEntryValid: nextIsAllValid});
        this.setState({validationInputFields: nextValidationInputFields});
        this.setState({validationDateFields: nextValidationDateFields});

        // go to next page once everything ok
        if (nextIsAllValid) {
            // this is synchronous by default..so the redirect will get the latest data from redux
            this.props.dispatch({
                type: 'SUBMISSION_REVIEW',
                payload: {
                    employeeName: this.props.employeeDetails.employeeName,
                    employeeNric: this.props.employeeDetails.employeeNric,
                    employeePassportNum: this.props.employeeDetails.employeePassportNum,
                    employeeCountryOrigin: this.props.employeeDetails.employeeCountryOrigin,
                    employeeCountryDestination: this.props.employeeDetails.employeeCountryDestination,
                    employeeTravelDateStart: this.props.employeeDetails.employeeTravelDateStart,
                    employeeTravelDateEnd: this.props.employeeDetails.employeeTravelDateEnd
                }
            });

            this.props.history.push({
                pathname: '/application-review',
            });
        }
    };

    renderInputFields = () => {
        return this.state.validationInputFields.map((x, index) => {
            const keyName = Object.keys(x)[0];
            const fieldDetails = Object.values(x)[0];
            const fieldName = fieldDetails[0];
            const isMandatory = fieldDetails[1];
            const isValid = fieldDetails[2];
            const errStr = fieldDetails[3];

            let errorDiv = null;
            let inputClassName = 'jd-form-field-input';

            if (!isValid) {
                errorDiv = <div className="jd-form-field-label-err">{errStr}</div>;
                inputClassName = 'jd-form-field-input jd-form-field-input-err';
            }

            return (
                <div key={index}>
                    <div className="jd-form-field-label">{isMandatory ? fieldName + '*' : fieldName}</div>
                    <input type="text"
                           className={inputClassName}
                           onChange={(e) => {
                               this.handleChange(e.target.value, keyName)
                           }}
                           value={this.props.employeeDetails[keyName]}
                    />
                    {errorDiv}
                </div>
            );
        });
    };

    renderDateFields = () => {
        return this.state.validationDateFields.map((x, index) => {
            const keyName = Object.keys(x)[0];
            const fieldDetails = Object.values(x)[0];
            const fieldName = fieldDetails[0];
            const isMandatory = fieldDetails[1];
            const isValid = fieldDetails[2];
            const errStr = fieldDetails[3];

            let errorDiv = null;
            let inputClassName = 'jd-form-field-input';

            if (!isValid) {
                errorDiv = <div className="jd-form-field-label-err">{errStr}</div>;
                inputClassName = 'jd-form-field-input jd-form-field-input-err';
            }

            return (
                <div key={index}>
                    <div className="jd-form-field-label">{isMandatory ? fieldName + '*' : fieldName}</div>
                    <DatePicker
                        className={inputClassName}
                        selected={this.props.employeeDetails[keyName]}
                        onChange={(dc) => {
                            this.handleChange(dc, keyName)
                        }}
                    />
                    {errorDiv}
                </div>
            );
        });
    };

    render() {
        return (
            <div>
                <CmpHeader currentPage="NewApplication"/>
                <div className="content">
                    <div className="siimple-grid">
                        <div className="siimple-grid-row">
                            <div className="siimple-grid-col siimple-grid-col--5 siimple-grid-col--sm-12">
                                <div className="jd-box">
                                    <div className="jd-form-title">Employee Details</div>
                                    {this.renderInputFields().map(x => x)}
                                    {this.renderDateFields().map(x => x)}
                                    <div className="jd-spacer"/>
                                    <div className="siimple-btn siimple-btn--primary"
                                         onClick={this.handleClickContinue}>Continue
                                    </div>
                                </div>
                            </div>
                            <div className="siimple-grid-col siimple-grid-col--1 siimple-grid-col--sm-12">
                            </div>
                            <div className="siimple-grid-col siimple-grid-col--3 siimple-grid-col--sm-12">
                                <div className="siimple-grid-row">
                                    <div className="card">
                                        <div className="card-container">
                                            <img className="card-img" src={"./images/account.png"} alt="avatar"/>
                                            <div className="card-title">Account Details</div>
                                            <div className="card-container">
                                                <CmpCompanyDetails companyDetails={this.props.companyDetails}/>
                                                <div className="jd-spacer"/>
                                                <CmpApplicatnDetails applicantDetails={this.props.applicantDetails}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = reduxState => {
    return {
        companyDetails: {
            companyName: reduxState.accountDetails.companyName,
            companyAddress: reduxState.accountDetails.companyAddress,
            companyUen: reduxState.accountDetails.companyUen
        },
        applicantDetails: {
            applicantName: reduxState.accountDetails.applicantName,
            applicantContact: reduxState.accountDetails.applicantContact,
            applicantEmail: reduxState.accountDetails.applicantEmail
        },
        employeeDetails: reduxState.employeeDetails,
        submissionUpdate: reduxState.submissionUpdate
    };
};

export default connect(mapStateToProps)(CntNewApplication);