import React, {Component} from 'react';
import {connect} from 'react-redux';
import CmpHeader from "../component/CmpHeader";
import CmpAccountDetails from "../component/CmpAccountDetails";
import jdUtil from "../jdUtil";
import _ from 'lodash';
import axios from "axios";

class CntAccount extends Component {
    constructor(props) {
        super(props);
        // save the initial state for update rollback during cancel
        this.state = {
            isModified: false,
            isValidCompanyName: true,
            isValidCompanyAddress: true,
            isValidCompanyUen: true,
            isValidApplicantName: true,
            isValidApplicantContact: true,
            isValidApplicantEmail: true
        }
    }

    retrieveFromGateway() {
        axios.get('/api/accounts').then((response) => {
            // to rollback after pressing cancel
            const initalStateAccount = _.cloneDeep(response.data);
            this.setState({
                accountDetails: initalStateAccount
            });
            this.props.dispatch({
                type: 'ACCOUNT_FROM_GW',
                payload: response.data
            });
        }).catch((error) => {
            console.log(error);
        })
    }

    componentDidMount() {
        this.retrieveFromGateway();
    }

    componentWillUnmount() {
        // definitely not the best way because of the 2-way binding
        // ensure account information is not change if user navigate away from the page without saving
        this.props.dispatch({
            type: 'ACCOUNT_RESTORE_INITIAL_STATE',
            payload: this.state.accountDetails
        });
    }

    // create a 2-way binding to redux
    handleChange = (value, stateKeyName) => {
        this.setState({
            isModified: true
        });
        this.props.dispatch({
            type: 'ACCOUNT_ENTRY_BINDING',
            payload: {
                stateKeyName: stateKeyName,
                inputValue: value
            }
        });
    };

    handleClickCancel = () => {
        this.props.dispatch({
            type: 'ACCOUNT_RESTORE_INITIAL_STATE',
            payload: this.state.accountDetails
        });
        this.setState({
            isModified: false,
            isValidCompanyName: true,
            isValidCompanyAddress: true,
            isValidCompanyUen: true,
            isValidApplicantName: true,
            isValidApplicantContact: true,
            isValidApplicantEmail: true
        });
    };

    handleClickSave = () => {
        let isAllEntryValid = true;
        let nextState = {
            isValidCompanyName: true,
            isValidCompanyAddress: true,
            isValidCompanyUen: true,
            isValidApplicantName: true,
            isValidApplicantContact: true,
            isValidApplicantEmail: true
        };

        if (jdUtil.isStringEmpty(this.props.accountDetails.companyName)) {
            nextState.isValidCompanyName = false;
            isAllEntryValid = false;
        }
        if (jdUtil.isStringEmpty(this.props.accountDetails.companyAddress)) {
            nextState.isValidCompanyAddress = false;
            isAllEntryValid = false;
        }
        if (this.props.accountDetails.companyUen && !jdUtil.isValidUen(this.props.accountDetails.companyUen)) {
            nextState.isValidCompanyUen = false;
            isAllEntryValid = false;
        }
        if (jdUtil.isStringEmpty(this.props.accountDetails.applicantName)) {
            nextState.isValidApplicantName = false;
            isAllEntryValid = false;
        }
        if (jdUtil.isStringEmpty(this.props.accountDetails.applicantContact)) {
            nextState.isValidApplicantContact = false;
            isAllEntryValid = false;
        }
        if (!jdUtil.isValidEmail(this.props.accountDetails.applicantEmail)) {
            nextState.isValidApplicantEmail = false;
            isAllEntryValid = false;
        }
        this.setState(nextState);

        if (isAllEntryValid) {
            const persistState = {
                username: this.props.accountDetails.username,
                companyName: this.props.accountDetails.companyName,
                companyAddress: this.props.accountDetails.companyAddress,
                companyUen: this.props.accountDetails.companyUen,
                applicantName: this.props.accountDetails.applicantName,
                applicantContact: this.props.accountDetails.applicantContact,
                applicantEmail: this.props.accountDetails.applicantEmail
            };
            // redux already has the latest state since we already did 2-way binding
            // just fire a command to write to mongo
            axios.post('/api/accounts', persistState).then((response) => {
                this.props.dispatch({
                    type: 'ACCOUNT_SAVE_MODIFICATION',
                    payload: persistState
                });
                // we also need to update our local component state so that cancel will fall back to the last saved state
                this.setState({
                    isModified: false,
                    accountDetails: persistState
                });
            }).catch((error) => {
                //console.log(error);
            })
        }
    };

    render() {
        let modificationButtons = null;
        if (this.state.isModified) {
            modificationButtons = (
                <div>
                    <div className="siimple-btn siimple-btn--error"
                         onClick={this.handleClickCancel}>Cancel
                    </div>
                    <div style={{marginLeft:'20px'}} className="siimple-btn siimple-btn--success"
                         onClick={this.handleClickSave}>Save
                    </div>
                </div>
            );
        }

        return (
            <div>
                <CmpHeader currentPage="Account"/>
                <div className="content">
                    <div className="siimple-grid">
                        <div className="siimple-grid-row">
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12" />
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12">
                                <div className="jd-box">
                                    <div className="jd-form-title">Update Account Details</div>
                                    <CmpAccountDetails accountDetails={this.props.accountDetails}
                                                       onChange={this.handleChange}
                                                       validationObj={this.state}
                                    />
                                    <div className="jd-spacer" />
                                    {modificationButtons}
                                </div>
                            </div>
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = reduxState => {
    return {
        accountDetails: reduxState.accountDetails
    };
};

export default connect(mapStateToProps)(CntAccount);