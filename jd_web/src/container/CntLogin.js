import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import axios from "axios";


class CntLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isFailLogin: false
        }
    }

    componentDidMount() {
    }

    handleChange = (value, stateKeyName) => {
        this.setState({
            [stateKeyName]: value
        })
    };

    handleClickSubmit = () => {
        // api will only return 200 on successful login..any other value is fail authentication
        axios.post('/api/authenticate', {
            username: this.state.username,
            password: this.state.password
        }).then((response) => {
            this.setState({
                isFailLogin: false
            })
            this.props.dispatch({
                type: 'LOGIN_UPDATE',
                payload: {
                    isLogin: true,
                    username: this.state.username
                }
            });
        }).catch((error) => {
            this.setState({
                isFailLogin: true
            })
            this.props.dispatch({
                type: 'LOGIN_UPDATE',
                payload: {
                    isLogin: false,
                    username: this.state.username
                }
            });
        })
    };

    render() {
        if (this.props.loginDetails.isLogin) {
            return <Redirect to="/"/>
        } else {
            return (
                <div className="content">
                    <div className="siimple-grid">
                        <div className="siimple-grid-row">
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12"/>
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12">
                                <div className="jd-box">
                                    <div className="jd-form-title">Sign in</div>
                                    <div className="jd-form-field-label">Username</div>
                                    <input type="text"
                                           className={this.state.isFailLogin ? "jd-form-field-input jd-form-field-input-err" : "jd-form-field-input"}
                                           onChange={(e) => {
                                               this.handleChange(e.target.value, 'username');
                                           }}
                                           placeholder="username is user1"
                                           value={this.state.username}
                                    />
                                    <div className="jd-form-field-label">Password</div>
                                    <input type="password"
                                           className={this.state.isFailLogin ? "jd-form-field-input jd-form-field-input-err" : "jd-form-field-input"}
                                           onChange={(e) => {
                                               this.handleChange(e.target.value, 'password');
                                           }}
                                           placeholder="passowrd is user1"
                                           value={this.state.password}
                                    />
                                    <div style={{marginTop: '20px'}} className="siimple-btn siimple-btn--primary"
                                         onClick={this.handleClickSubmit}>Submit
                                    </div>
                                </div>
                            </div>
                            <div className="siimple-grid-col siimple-grid-col--4 siimple-grid-col--sm-12"/>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = reduxState => {
    return {
        loginDetails: reduxState.loginDetails
    }
};

export default connect(mapStateToProps)(CntLogin);