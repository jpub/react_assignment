import jdUtil from "../jdUtil";

const initialState = {
    employeeName: '',
    employeeNric: '',
    employeePassportNum: '',
    employeeCountryOrigin: '',
    employeeCountryDestination: '',
    employeeTravelDateStart: jdUtil.getDate(0),
    employeeTravelDateEnd: jdUtil.getDate(0)
};

const rdcEmployeeDetails = (state = initialState, action) => {
    const apd = action.payload;
    switch (action.type) {
        case 'SUBMISSION_REVIEW': {
            return ({
                employeeName: apd.employeeName,
                employeeNric: apd.employeeNric,
                employeePassportNum: apd.employeePassportNum,
                employeeCountryOrigin: apd.employeeCountryOrigin,
                employeeCountryDestination: apd.employeeCountryDestination,
                employeeTravelDateStart: apd.employeeTravelDateStart,
                employeeTravelDateEnd: apd.employeeTravelDateEnd
            });
        }
        case 'SUBMISSION_UPDATE': {
            return ({
                employeeName: apd.employeeName,
                employeeNric: apd.employeeNric,
                employeePassportNum: apd.employeePassportNum,
                employeeCountryOrigin: apd.employeeCountryOrigin,
                employeeCountryDestination: apd.employeeCountryDestination,
                employeeTravelDateStart: jdUtil.getDateFromStr(apd.employeeTravelDateStart),
                employeeTravelDateEnd: jdUtil.getDateFromStr(apd.employeeTravelDateEnd)
            });
        }
        case 'EMPLOYEE_ENTRY_BINDING': {
            // const nextState = {};
            // nextState[stateKeyName] = e.target.value;
            // // object literal cannot use key var
            // this.setState(nextState);
            return {...state, [apd.stateKeyName]: apd.inputValue};
        }
        case 'EMPLOYEE_CLEAR_ENTRY': {
            return initialState;
        }
        default:
            return state;
    }
};

export default rdcEmployeeDetails;