import jdUtil from "../jdUtil";

const initialState = {
    submissionDate: "---",
    submissionId: -1,
    submissionStatus: 'NA',
    submissionAmount: -1,
    submissionDueOn: "---",
    submissionPayOn: "---",
};

const rdcSubmissionUpdate = (state=initialState, action) => {
    const apd = action.payload;
    switch (action.type) {
        case 'SUBMISSION_UPDATE': {
            console.log(apd);
            return ({
                submissionDate: jdUtil.getDateFromStr(apd.submissionDate),
                submissionId: apd.submissionId,
                submissionStatus: apd.submissionStatus,
                submissionAmount: apd.submissionAmount,
                submissionDueOn: jdUtil.getDateFromStr(apd.submissionDueOn),
                submissionPayOn: apd.submissionPayOn,
            });
        }
        case 'EMPLOYEE_CLEAR_ENTRY': {
            return initialState;
        }
        default:
            return state;
    }
};

export default rdcSubmissionUpdate;