// const initialState = [
//     {
//         username: "user1",
//         submissionDate: "02-Nov-19",
//         submissionId: 1,
//         submissionStatus: 'pending',
//         submissionAmount: 2300,
//         submissionDueOn: "30-Dec-19",
//         submissionPayOn: "---",
//         applicantName: 'Tan Ah Boon, Matthew',
//         applicantContact: '9732 2341',
//         applicantEmail: 'tanab@ncb.com.sg',
//         companyName: 'National Crop Biz',
//         companyAddress: '5 Changi Park, Block A, Singapore 623521',
//         companyUen: 'T09LL0001B',
//         employeeName: 'Ng Chen Keng, James',
//         employeeNric: 'S7263512G',
//         employeePassportNum: 'T99232342',
//         employeeCountryOrigin: 'Singapore',
//         employeeCountryDestination: 'Hong Kong',
//         employeeTravelDateStart: "15-Dec-19",
//         employeeTravelDateEnd: "18-Dec-19"
//     },
//     {
//         username: "user1",
//         submissionDate: "05-Sep-19",
//         submissionId: 2,
//         submissionStatus: 'approve',
//         submissionAmount: 1800,
//         submissionDueOn: "13-Sep-19",
//         submissionPayOn: "10-Sep-19",
//         applicantName: 'Tan Ah Boon, Matthew',
//         applicantContact: '9732 2341',
//         applicantEmail: 'tanab@ncb.com.sg',
//         companyName: 'National Crop Biz',
//         companyAddress: '5 Changi Park, Block A, Singapore 623521',
//         companyUen: 'T09LL0001B',
//         employeeName: 'Ng Beng Hwa, John',
//         employeeNric: 'S7912432H',
//         employeePassportNum: 'T88554422',
//         employeeCountryOrigin: 'Singapore',
//         employeeCountryDestination: 'Taiwan',
//         employeeTravelDateStart: "20-Sep-19",
//         employeeTravelDateEnd: "25-Sep-19"
//     }
// ];

const initialState = [];

const rdcSubmissionDetails = (state = initialState, action) => {
    const apd = action.payload;
    switch (action.type) {
        case 'SUBMISSION_FROM_GW': {
            return apd;
        }
        case 'SUBMISSION_NEW_ENTRY': {
            let entryIndex = state.findIndex(x => {
                return x.submissionId === apd.submissionId;
            });
            // this is a new entry
            if (entryIndex === -1) {
                return [apd, ...state];
            } else {
                return state.map(x => {
                    if (x.submissionId === apd.submissionId) {
                        return {...apd};
                    } else {
                        return x;
                    }
                });
            }
        }
        default:
            return state;
    }
};

export default rdcSubmissionDetails;