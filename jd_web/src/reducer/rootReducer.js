import {combineReducers} from "redux";
import accountDetails from "./rdcAccountDetails"
import employeeDetails from "./rdcEmployeeDetails";
import loginDetails from "./rdcLoginDetails";
import submissionDetails from "./rdcSubmissionDetails";
import submissionUpdate from './rdcSubmissionUpdate';

export default combineReducers({
    accountDetails,
    employeeDetails,
    loginDetails,
    submissionDetails,
    submissionUpdate
})