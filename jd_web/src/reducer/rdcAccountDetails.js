const initialState = {
    username: '',
    companyName: '',
    companyAddress: '',
    companyUen: '',
    applicantName: '',
    applicantContact: '',
    applicantEmail: ''
};

const rdcAccountDetails = (state=initialState, action) => {
    const apd = action.payload;
    switch (action.type) {
        case 'ACCOUNT_FROM_GW': {
            return apd;
        }
        case 'ACCOUNT_ENTRY_BINDING': {
            return {...state, [apd.stateKeyName]:apd.inputValue};
        }
        case 'ACCOUNT_RESTORE_INITIAL_STATE': {
            return {...state, ...apd};
        }
        case 'ACCOUNT_SAVE_MODIFICATION': {
            // fire axios to backend...todo
            return {...state, ...apd};
        }
        default:
            return state;
    }
};

export default rdcAccountDetails;