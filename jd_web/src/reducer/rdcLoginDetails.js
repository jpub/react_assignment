const initialState = {
    isLogin: false,
    username: null
};

const rdcLoginDetails = (state=initialState, action) => {
    const apd = action.payload;
    switch (action.type) {
        case 'LOGIN_UPDATE': {
            return {...apd};
        }
        default:
            return state;
    }
};

export default rdcLoginDetails;