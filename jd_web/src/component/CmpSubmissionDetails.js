import React from 'react';
import jdUtil from "../jdUtil";

const CmpSubmissionDetails = props => {
    const inputClassName = 'jd-form-field-input';
    let jsxToDisplay = null;
    if (props.submissionDetails) {
        jsxToDisplay = (
            <div>
                <div className="jd-form-field-label">ID</div>
                <input type="text" className={inputClassName} value={props.submissionDetails.submissionId} readOnly/>
                <div className="jd-form-field-label">Date Submitted</div>
                <input type="text"
                       className={inputClassName}
                       value={jdUtil.getDisplayDateFromStr(props.submissionDetails.submissionDate)}
                       readOnly/>
                <div className="jd-form-field-label">Total Payable</div>
                <input type="text" className={inputClassName} value={jdUtil.getDisplayCurrency(props.submissionDetails.submissionAmount)} readOnly/>
                <div className="jd-form-field-label">Payment Due Date</div>
                <input type="text"
                       className={inputClassName}
                       value={jdUtil.getDisplayDateFromStr(props.submissionDetails.submissionDueOn)}
                       readOnly/>
                <div className="jd-form-field-label">Paid On</div>
                <input type="text"
                       className={inputClassName}
                       value={jdUtil.getDisplayDateFromStr(props.submissionDetails.submissionPayOn)}
                       readOnly/>
            </div>
        );
    }
    return jsxToDisplay;
};

export default CmpSubmissionDetails;