import React from 'react';
import DatePicker from "react-datepicker";


const CmpEmployeeDetails = props => {
    const inputClassName = 'jd-form-field-input';
    return (
        <div>
            <div className="jd-form-field-label">Name</div>
            <input type="text" className={inputClassName} value={props.employeeDetails.employeeName} readOnly/>
            <div className="jd-form-field-label">NRIC</div>
            <input type="text" className={inputClassName} value={props.employeeDetails.employeeNric} readOnly/>
            <div className="jd-form-field-label">Passport Number</div>
            <input type="text" className={inputClassName} value={props.employeeDetails.employeePassportNum} readOnly/>
            <div className="jd-form-field-label">Country of Origin</div>
            <input type="text" className={inputClassName} value={props.employeeDetails.employeeCountryOrigin} readOnly/>
            <div className="jd-form-field-label">Country of Destination</div>
            <input type="text" className={inputClassName} value={props.employeeDetails.employeeCountryDestination} readOnly/>
            <div className="jd-form-field-label">Period from</div>
            <DatePicker
                className={inputClassName}
                selected={props.employeeDetails.employeeTravelDateStart}
                readOnly
            />
            <div className="jd-form-field-label">Period to</div>
            <DatePicker
                className={inputClassName}
                selected={props.employeeDetails.employeeTravelDateEnd}
                readOnly
            />
        </div>
    );
};

export default CmpEmployeeDetails;