import React from 'react';

const CmpCompanyDetails = props => {
    return (
        <div className="jd-box">
            <div className="card-container-title">Company</div>
            <hr/>
            <div className="card-container-description">{props.companyDetails.companyName}</div>
            <div className="card-container-description">{props.companyDetails.companyAddress}</div>
            <div className="card-container-description">{props.companyDetails.companyUen}</div>
        </div>
    )
};

export default CmpCompanyDetails;