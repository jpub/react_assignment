import React from 'react';


const CmpAccountDetails = props => {
    const inputClassName = 'jd-form-field-input';
    const errClassName = 'jd-form-field-input jd-form-field-input-err';
    const fieldErrMandatory = <div className="jd-form-field-label-err">Mandatory field</div>;
    const fieldErrInvalidFormat = <div className="jd-form-field-label-err">Invalid format</div>;
    return (
        <div>
            <div className="jd-form-field-label">Company Name*</div>
            <input type="text"
                   className={props.validationObj.isValidCompanyName === true ? inputClassName : errClassName}
                   value={props.accountDetails.companyName}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'companyName')
                   }}
            />
            {props.validationObj.isValidCompanyName === true ? null : fieldErrMandatory}
            <div className="jd-form-field-label">Company Address*</div>
            <input type="text"
                   className={props.validationObj.isValidCompanyAddress === true ? inputClassName : errClassName}
                   value={props.accountDetails.companyAddress}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'companyAddress')
                   }}
            />
            {props.validationObj.isValidCompanyAddress === true ? null : fieldErrMandatory}
            <div className="jd-form-field-label">Company UEN</div>
            <input type="text"
                   className={props.validationObj.isValidCompanyUen === true ? inputClassName : errClassName}
                   value={props.accountDetails.companyUen}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'companyUen')
                   }}
            />
            {props.validationObj.isValidCompanyUen === true ? null : fieldErrInvalidFormat}
            <div className="jd-form-field-label">Applicant Name*</div>
            <input type="text"
                   className={props.validationObj.isValidApplicantName === true ? inputClassName : errClassName}
                   value={props.accountDetails.applicantName}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'applicantName')
                   }}
            />
            {props.validationObj.isValidApplicantName === true ? null : fieldErrMandatory}
            <div className="jd-form-field-label">Applicant Contact*</div>
            <input type="text"
                   className={props.validationObj.isValidApplicantContact === true ? inputClassName : errClassName}
                   value={props.accountDetails.applicantContact}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'applicantContact')
                   }}
            />
            {props.validationObj.isValidApplicantContact === true ? null : fieldErrMandatory}
            <div className="jd-form-field-label">Applicant Email*</div>
            <input type="text"
                   className={props.validationObj.isValidApplicantEmail === true ? inputClassName : errClassName}
                   value={props.accountDetails.applicantEmail}
                   onChange={(e) => {
                       props.onChange(e.target.value, 'applicantEmail')
                   }}
            />
            {props.validationObj.isValidApplicantEmail === true ? null : fieldErrInvalidFormat}
        </div>
    );
};

export default CmpAccountDetails;