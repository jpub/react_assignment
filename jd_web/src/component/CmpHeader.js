import React from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import Cookies from 'js-cookie';

const CmpHeader = props => {
    const myFunction = () => {
        var x = window.document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    };

    const clearJwtToken = () => {
        Cookies.remove('jwtPC');
        props.dispatch({
            type: 'LOGIN_UPDATE',
            payload: {
                isLogin: false,
                username: ''
            }
        });
    };

    return (
        <div className="topnav" id="myTopnav">
            <Link className={props.currentPage === 'Submission' ? 'active' : ''}
                  to="/">Submission
            </Link>
            <Link className={props.currentPage === 'NewApplication' ? 'active' : ''}
                  to="/application">New application
            </Link>
            <Link className={props.currentPage === 'Account' ? 'active' : ''}
                  to="/account">Account
            </Link>
            <div className="topnav-right">
                <Link className=""
                      onClick={clearJwtToken}
                      to="/login">Logout
                </Link>
            </div>
            <Link className="icon" onClick={myFunction} to="#">
                <i className="fa fa-bars"/>
            </Link>
        </div>
    );
};

const mapStateToProps = reduxState => {
    return {};
};

// connect to redux store from navbar since user can logout from any page
export default connect(mapStateToProps)(CmpHeader);