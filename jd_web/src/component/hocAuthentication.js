import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios';

const hocAuthentication = (WrappedComponent) => {
    return class extends Component {
        constructor(props) {
            super(props);
            this.state = {
                loading: true,
                redirect: false
            };
        }

        componentDidMount() {
            // protected component will always need to ensure jwt is still valid
            axios.get('/api/validateToken').then((response) => {
                if (response.status === 200) {
                    this.setState({
                        loading: false,
                        redirect: false
                    });
                } else {
                    throw(new Error(response.error));
                }
            }).catch((error) => {
                console.log(error);
                this.setState({
                    loading: false,
                    redirect: true
                });
            })
        }

        render() {
            if (this.state.loading) {
                return null;
            }
            if (this.state.redirect) {
                return <Redirect to="/login"/>
            }
            return (
                <React.Fragment>
                    <WrappedComponent {...this.props} />
                </React.Fragment>
            );
        }
    }
};

export default hocAuthentication;