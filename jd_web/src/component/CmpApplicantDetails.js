import React from 'react';

const CmpApplicatnDetails = props => {
    return (
        <div className="jd-box">
            <div className="card-container-title">Applicant</div>
            <hr/>
            <div className="card-container-description">{props.applicantDetails.applicantName}</div>
            <div className="card-container-description">{props.applicantDetails.applicantContact}</div>
            <div className="card-container-description">{props.applicantDetails.applicantEmail}</div>
        </div>
    )
};

export default CmpApplicatnDetails;