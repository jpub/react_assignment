/**
 * UEN validation is taken directly from https://gist.github.com/mervintankw/90d5660c6ab03a83ddf77fa8199a0e52
 *
 */
var moment = require('moment');
var validator = require('validator');

const jdUtil = (function () {
    var mylib;

    const init = () => {
        mylib = {};
        mylib.getDate = (offsetDays) => {
            return moment(new Date()).add(offsetDays, 'days').toDate();
        };

        mylib.getDateFromStr = (inStr) => {
            return moment(new Date(inStr)).toDate();
        };

        mylib.getRandom = (min, max) => {
            return Math.random() * (max - min) + min;
        };

        mylib.getDisplayDate = (inDate) => {
            if (inDate === null || !moment(inDate).isValid())
                return '';
            return moment(inDate).format('DD-MMM-YY');
        };

        mylib.getDisplayDateFromStr = (inStr) => {
            if (inStr === '---')
                return '';
            // without the "new Date"..the console if full of warnings...but this new really looks so redundant
            return moment(new Date(inStr)).format('DD-MMM-YY');
        };

        mylib.getDisplayCurrency = (inValue) => {
            return '$' + (inValue).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        };

        mylib.isStringEmpty = (inStr) => {
            return !(/\S/.test(inStr));
        };

        mylib.isValidEmail = (inEmail) => {
            return validator.isEmail(inEmail);
        };

        mylib.isValidUen = (uen) => {
            var regex_nnnnnnnnX = new RegExp('^[0-9]{8}[A-Z]', 'g');
            var regex_yyyynnnnnX = new RegExp('^[0-9]{9}[A-Z]', 'g');
            var regex_TyyPQnnnnX = new RegExp('^[TRS][0-9]{2}[A-Z]{2}[0-9]{4}[A-Z]', 'g');
            const PQ_indicator = [
                'LP', 'LL', 'FC', 'PF', 'RF', 'MQ', 'MM', 'NB', 'CC', 'CS', 'MB', 'FM', 'GS', 'GA',
                'GB', 'DP', 'CP', 'NR', 'CM', 'CD', 'MD', 'HS', 'VH', 'CH', 'MH', 'CL', 'XL', 'CX',
                'RP', 'TU', 'TC', 'FB', 'FN', 'PA', 'PB', 'SS', 'MC', 'SM'
            ];

            uen = uen.toUpperCase();
            if (uen.length === 9) {
                return regex_nnnnnnnnX.test(uen);
            } else if (uen.length === 10) {
                if (regex_yyyynnnnnX.test(uen))
                    return true;
                if (regex_TyyPQnnnnX.test(uen)) {
                    return PQ_indicator.indexOf(uen.slice(3,5)) === -1 ? false : true;
                }
                return false;
            } else {
                return false;
            }
        };
        return mylib;
    };

    if (!mylib) {
        mylib = init();
    }
    return mylib;
})();

export default jdUtil;