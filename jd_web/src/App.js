import React from 'react';
import {Route, BrowserRouter as Router} from 'react-router-dom';
import hocAuthentication from "./component/hocAuthentication";
import CntLogin from './container/CntLogin'
import CntAccount from './container/CntAccount'
import CntDashBoard from "./container/CntDashBoard";
import CntNewApplication from "./container/CntNewApplication";
import CntNewApplicationReview from "./container/CntNewApplicationReview";


const App = () => {
    return (
        <Router>
            {/*<Route path="/" exact component={CntLogin} />*/}
            {/*<Route path="/newapplication" component={CntNewApplication} />*/}
            <Route path="/login" exact component={CntLogin} />
            <Route path="/" exact component={hocAuthentication(CntDashBoard)} />
            <Route path="/application" exact component={hocAuthentication(CntNewApplication)} />
            <Route path="/application-review" exact component={hocAuthentication(CntNewApplicationReview)} />
            <Route path="/account" exact component={hocAuthentication(CntAccount)} />
        </Router>
    );
};

export default App;

// const App = props => {
//     return (
//         <div>
//             <div>Header</div>
//             <Applications />
//             <div>Footer</div>
//         </div>
//     );
// };