# React Assignment

## Running locally
Clone repository
```
git clone https://gitlab.com/jpub/react_assignment.git
```
Start API gateway
```
cd jd_app
npm install
npm start
```
Start React developement server
```
cd jd_web
npm install
npm start
```
Access to web portal
[http://localhost:3000/login](http://localhost:3000/login)

Login credentials
```
username: user1
password: user1
```
Database is hosted on **MongoDB Atlas**

## Demo server
Running instance in Azure Cloud
[http://13.67.66.73:3000/login](http://13.67.66.73:3000/login)

## Todo
- Unit testing
- Shift deployment to Docker containers
- Explore redux-thunk
- Explore hooks
- Functional 
     - clean up all the actions with action creator
     - encrypt password
     - payment gateway
     - explore springboot as backend
